# Glossary
Here are all the important concepts, words to know to understand easier the project flow

## Kubernetes

#### Minikube
Tool that sets up a local Kubernetes cluster on macOS, Linux, and Windows. We proudly focus on helping application developers and new Kubernetes users.

#### Rancher desktop
An open-source application that provides all the essentials to work with containers and Kubernetes on the desktop.

#### Kubectl
Command line tool for communicating with a Kubernetes cluster's control plane, using the Kubernetes API.

#### Namespace
Namespaces provide a mechanism for isolating groups of resources within a single cluster. Names of resources need to be unique within a namespace, but not across namespaces.

#### Custom resource definition (CRD)
Custom resources are extensions of the Kubernetes API. This page discusses when to add a custom resource to your Kubernetes cluster and when to use a standalone service.

#### Custom object (CO)
Instance of a custom resource definition (CRD).

#### Secret
A Secret is an object that contains a small amount of sensitive data such as a password, a token, or a key. Such information might otherwise be put in a Pod specification or in a container image.

#### Role based access control (RBAC)
Role-based access control (RBAC) is a method of regulating access to computer or network resources based on the roles of individual users within your organization.

RBAC authorization uses the rbac.authorization.k8s.io API group to drive authorization decisions, allowing you to dynamically configure policies through the Kubernetes API.

#### Pod
A Pod (as in a pod of whales or pea pod) is a group of one or more containers, with shared storage and network resources, and a specification for how to run the containers.

#### K8s sidecar container
Kubernetes CSI Sidecar Containers are a set of standard containers that aim to simplify the development and deployment of CSI Drivers on Kubernetes.

These containers contain common logic to watch the Kubernetes API, trigger appropriate operations against the “CSI volume driver” container, and update the Kubernetes API as appropriate.

#### Csi objects
The Kubernetes API contains the following CSI specific objects:
- CSIDriver Object
- CSINode Object

#### Configmap
A ConfigMap is an API object used to store non-confidential data in key-value pairs. Pods can consume ConfigMaps as environment variables, command-line arguments, or as configuration files in a volume.

## Fuse

#### File system
File system or filesystem (often abbreviated to FS or fs) governs file organization and access. A local file system is a capability of an operating system that services the applications running on the same computer.

## Csi driver

#### Kubelet plugin registration
This folder contains a utility, pluginwatcher, for Kubelet to register different types of node-level plugins such as device plugins or CSI plugins. It discovers plugins by monitoring inotify events under the directory returned by kubelet.getPluginsDir(). We will refer to this directory as PluginsDir.

#### Unix domain socket (UDS)
Unix domain socket aka UDS or IPC socket (inter-process communication socket) is a data communications endpoint for exchanging data between processes executing on the same host operating system.

#### Node-driver-registrar
The CSI node-driver-registrar is a sidecar container that fetches driver information (using NodeGetInfo) from a CSI endpoint and registers it with the kubelet on that node using the kubelet plugin registration mechanism.

#### K8-sidecontainer
Kubernetes CSI Sidecar Containers are a set of standard containers that aim to simplify the development and deployment of CSI Drivers on Kubernetes.

These containers contain common logic to watch the Kubernetes API, trigger appropriate operations against the “CSI volume driver” container, and update the Kubernetes API as appropriate.

The containers are intended to be bundled with third-party CSI driver containers and deployed together as pods.

## Others

#### Gitops
GitOps uses Git repositories as a single source of truth to deliver infrastructure as code. Submitted code checks the CI process, while the CD process checks and applies requirements for things like security, infrastructure as code, or any other boundaries set for the application framework.

#### Git
Git is a free and open source distributed version control system designed to handle everything from small to very large projects with speed and efficiency.

## References
- [kubernetes](https://kubernetes.io/)
- [kubernetes_csi](https://kubernetes-csi.github.io/docs/introduction.html)
- [red_hat](https://www.redhat.com/en/topics/devops/what-is-gitops)
- [minikube](https://minikube.sigs.k8s.io/docs/)
- [rancher_desktop](https://rancherdesktop.io/)
- [git](https://git-scm.com/)


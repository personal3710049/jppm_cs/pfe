# Fuse
This is our main container, the one in charge of fetching the data from the datasource, check for the custom object [**CO**](././glosary.md)  updates and exposing in a secure manner this data retrieved by a [**fuse filesystem**](././glosary.md) process.
## What is fuse?
Filesystem in USErspace (FUSE) is a software interface for Unix and Unix-like computer operating systems that lets non-privileged users create their file systems without editing kernel code. This is achieved by running file system code in user space while the FUSE module provides only a bridge to the actual kernel interfaces. (got from [website](https://medium.com/@goamaral/fuse-filesystem-b44768f27aa2))

## why fuse?
In order to been able to acts like a configmap, a fuse process can give us the possibility to expose data at run time thorugh a [**filesystem mechanism**](././glosary.md) that can be injected to pods thanks to a [**csi driver**](././glosary.md), giving the impression of a configmap with a higher size capability.
## High level design
### Static diagrams
#### Package diagram
![package_diagram](./img/package_diagram.png)
#### Class diagram
![class_diagram](./img/classDiagram.png)
### Dynamic diagrams
#### Sequence diagram
![sequence_diagram](./img/sequence_diagram.png)



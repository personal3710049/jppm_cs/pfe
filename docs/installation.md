# Installation
These are the differents commands for quick build, test and development.
For its correct execution, **execute them from the script directory**.
## Local
### Build
 This script is responsible for building the fuse container image. This can be done either in minikube or rancher desktop.

   - Minikube

     If use use minikube for building the fuse image inside minikube execute the following command:

        ```
        #remember been in ./scripts
        ./mkbuild.sh
        ```

    
   - Rancher desktop
    
    If you use rancher desktop for building the fuse image execute the following command:

        ```
        # Remember been in ./scripts
        ./build.sh
        ```

!-- For experimenting purposes -->
### Deploy
This script will pass all the yaml files to be deployed in the cluster by using [**kubectl**](./glosary.md) cli 
```
# Remember been in ./scripts
./deploy.sh
```
### Undeploy
this script will remove all the yaml files deployed when executing  ```./deploy.sh ```
```
 # Remember been in ./scripts
 ./undeploy.sh
```
### Fuse implementation 
This script will allow you to test your fuse implementation and other functionalities done by this container
```
# Remember been in ./scripts
./fuse_impl.sh

```

## Remote
Not yet fully implemented, Putting images in registry not done.



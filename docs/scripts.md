# Scripts
These are the differents scripts for quick build, test and development.

## variables
This script has all the global variables used in the different scripts
```
# paths
rootPath="$(dirname $PWD)"
scriptPath="$rootPath/scripts"
containerPath="$rootPath/customDockerImage/go"
k8sFilesPath="$rootPath/k8sFiles/"
csiDriverPath="$rootPath/meta-fuse-csi-plugin"

#docker
imageName="go-backend"
imageFuse="starter:latest"

# deployed to the k8s cluster
namespace="repospace"
CR="crdreporetriever.yaml"
CO="coreporetriever.yaml"
DEPLOYMENT="testDeployment.yaml"
ROLE="reporetrieverrole.yaml"
ROLESECRET="secretretrieverrole.yaml"
SECRET="keys"
secret="keys"
key_1="private_key"
key_2="known_hosts"
key_3="passphrase"
# colors
ORANGE='\033[0;33m'
YELLOW='\033[0;35m'
GREEN='\033[0;32m'
NC='\033[0m'
```
## build
This is the script for building the fuse image in your local machine if using [**rancher**](./glosary.md)
```
#!/bin/bash
# . ./scripts/variables.sh
. ./variables.sh
build(){
cd $containerPath
docker build -t $imageName .
cd $scriptPath

}
build
```
## mkbuild
This is the script for building the fuse image in  your local machine if using [**minikube**](./glosary.md)
```
#!/bin/bash
. ./variables.sh
createContainerImageInMinikube(){
   # for context 
    eval $(minikube docker-env)
    cd $containerPath
    docker build -t $imageName .
    eval $(minikube docker-env -u)
}
startMinikube(){
    if isMinikubeRunning ;
    then 
        echo -e "\n"
        echo -e "${ORANGE}                     ------------------------------                    "
        echo -e "${ORANGE}                     |      Starting Minikube     |                      ${NC}"
        echo -e "${ORANGE}                     ------------------------------                    "
        echo -e "${NC}"
        minikube start
    else
        echo -e "\n"
        echo -e "${ORANGE}                     -----------------------------                    "
        echo -e "${ORANGE}                     |      Already running      |                      ${NC}"
        echo -e "${ORANGE}                     -----------------------------                    "
        echo -e "${NC}"
    fi
}
isMinikubeRunning(){
    minikubeStatus=$(minikube status)
    if ! echo "$minikubeStatus" | grep -q "host: Running";
    then
        return 0
    else 
        return 1
    fi
}
build(){
    startMinikube  &&
    createContainerImageInMinikube &&
    cd $scriptPath
}
build
```
## Deploy
This script is for deploying all the different kubernetes objects through kubectl
```
#!/bin/bash
. ./variables.sh
suggestCommand(){
    echo -e "kubectl -n repospace create secret generic keys --from-file=privateKey=<PRIVATE_SSH_KEY_PATH> --from-file=knownHosts=<KNOWNHOST_PATH> --from-file=pvtKey=<PRIVATE_KEY_FILE_PATH> --from-literal=passphrase=\"\""
    echo -e "${NC}"
}
checkAccessToken(){
    secrets=$(kubectl -n $namespace get secrets )
    if echo "$secrets" | grep -q "$secret";
    then
        return 0
    else 
        return 1
    fi

}
existNamespace(){
    namespaces=$(kubectl get namespaces)
    if echo "$namespaces" | grep -q $namespace;
    then
        return 0
    else
        return 1
    fi

}
kubectlCommands(){
                echo -e "\n"
                echo -e "${ORANGE}                     --------------------------------------                "
                echo -e "${ORANGE}                     |      Applying kubectl commands     |                      ${NC}"
                echo -e "${ORANGE}                     --------------------------------------                "
                echo -e "${NC}"
                kubectl -n $namespace apply -f ./$CR &&  
                kubectl -n $namespace apply -f ./$CO &&
                # kubectl -n $namespace apply -f ./$DEPLOYMENT &&
                kubectl -n $namespace apply -f ./$ROLE &&
                kubectl -n $namespace apply -f ./$ROLESECRET
}
deploy(){
            if ! existNamespace; 
            then
                kubectl create namespace $namespace 
            fi
            cd $k8sFilesPath
            if checkAccessToken;
            then
                kubectlCommands
        else
            suggestCommand
            read -r accessCommand
            command $accessCommand
            kubectlCommands
        fi
        cd $scriptPath
}
deploy
```
## fuse impl
This is for deploying a single pod with a single container that is running the fuse but not exposing it 
(good for testing the implementation within the cluster)
```
#!/bin/bash
. ./variables.sh
run(){
. ./mkbuild.sh
. ./deploy.sh
cd $k8sFilesPath
kubectl -n $namespace apply -f testDeployment.yaml
cd $rootPath
}
run
```


